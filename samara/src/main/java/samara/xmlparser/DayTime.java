package samara.xmlparser;
import java.time.LocalDateTime;

public class DayTime {
	private LocalDateTime input;
	private LocalDateTime output;
	
	private Integer  minit;
	

	public LocalDateTime getInput() {
		return input;
	}
	public DayTime() {
		super();
		this.input = LocalDateTime.now();
		this.output = LocalDateTime.of(1970, 1, 1, 0, 0);
	}
	public void setInput(LocalDateTime ld) {
      if(input.isAfter(ld)) {
    	  this.input = ld;
      }
	}
	public LocalDateTime getOutput() {
		return output;
	}
	public void setOutput(LocalDateTime ld) {
		if(this.output.isBefore(ld)) {
			this.output = ld;
		}
	}
	public Integer getMinit() {
		return minit;
	}

	public void setMinit(Integer minit) {
		this.minit = minit;
	}

}
