package samara.xmlparser;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class EmployeeVisit {
	private String firstName;
	private String midleName;
	private String lastName;
	private String departament;
	private String org;

	private Map<Integer, DayTime> dayTime;

	public EmployeeVisit(String firstName, String midleName, String lastName, String departament) {
		super();
		this.firstName = firstName;
		this.midleName = midleName;
		this.lastName = lastName;
		this.departament = departament;
		this.dayTime = new LinkedHashMap<>();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMidleName() {
		return midleName;
	}

	public void setMidleName(String midleName) {
		this.midleName = midleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDepartament() {
		return departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public EmployeeVisit() {
		this.dayTime = new TreeMap<>();

	}

	public Map<Integer, DayTime> getDayTime() {
		return dayTime;
	}

	public void setDayTimeIn(LocalDateTime ld) {
		if (this.dayTime.containsKey(ld.getDayOfYear())) {
			this.dayTime.get(ld.getDayOfYear()).setInput(ld);
			
		} else {
			DayTime cur = new DayTime();
			cur.setInput(ld);
			// System.out.println("Input : " + cur.getInput().getHour());
			this.dayTime.put(ld.getDayOfYear(), cur);
		}
	}
      
	public void setDayTimeOut(LocalDateTime ld) {
		if (this.dayTime.containsKey(ld.getDayOfYear())) {
			this.dayTime.get(ld.getDayOfYear()).setOutput(ld);
		} else {
			DayTime cur = new DayTime();
			this.dayTime.put(ld.getDayOfYear(), cur);
		}
	}

}
