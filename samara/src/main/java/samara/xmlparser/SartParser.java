package samara.xmlparser;

import samara.exel.ExelTableWriteDoc;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import samara.DTO.DTOExelTabel;

public class SartParser implements  Runnable{
    public DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private String fileNameDowload;
    private String fileNameSave;
    private String fileName1C;
    private Long nornWork;
	public  void convert() {
            XMLParser pars = new XMLParser();
            pars.setFilename(fileNameDowload);
            pars.setNormTimeMonth(nornWork);
            pars.setFileName1C(fileName1C);
            pars.start();
            int row = 1;
            ExelTableWriteDoc wdoc = new ExelTableWriteDoc();
            wdoc.setFileName(this.fileNameSave);
            List<DTOExelTabel> dtEx = pars.DTOExelEmployee();
            Map<String, List<DTOExelTabel>> mapOrg = new HashMap<>();
        // сортировка сотрудников по отделам
            for(DTOExelTabel dt : dtEx) {
        	if(mapOrg.containsKey(dt.getOrgUnit())) {
        		mapOrg.get(dt.getOrgUnit()).add(dt);
                    }else {
        		List<DTOExelTabel> dtNew = new LinkedList<>();
        		dtNew.add(dt);
        		mapOrg.put(dt.getOrgUnit(),dtNew);
                    }
            }
        
            wdoc.createFile();
            wdoc.writeHead(pars.getFirstDate(), pars.getEndDate());
            for(Map.Entry<String, List<DTOExelTabel>> ent : mapOrg.entrySet()) {
        	wdoc.wirteOrgUnit(ent.getKey(), row);
        	row++;
        	List<DTOExelTabel> lDto = ent.getValue();
        	for(DTOExelTabel dtUn : lDto){
                    wdoc.writeRow(pars.getFirstDate(), pars.getEndDate(), row, dtUn);
                    row+=3;              
                }
        	wdoc.closeFile(); 
            }
    }

    public SartParser(String dowLoadFilePatch, String saveFilePatch, String FileName1C, Long workNorm) {
        this.fileNameSave  = saveFilePatch;
        this.fileNameDowload = dowLoadFilePatch;
        this.fileName1C = FileName1C;
        this.nornWork = workNorm;
    }

    @Override
    public void run() {
        this.convert();
    }

}
