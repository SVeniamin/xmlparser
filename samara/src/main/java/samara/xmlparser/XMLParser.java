package samara.xmlparser;

import samara.text1CParser.StaffNotWork;
import samara.text1CParser.Text1CParser;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import samara.DTO.DTOExelTabel;
import samara.constant.AlarmTime;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author admin
 */
public class XMLParser {
    private final String Out = "Штатныйвыход"; // "Штатный выход".equalsIgnoreCase(reader.getAttributeValue(i)) && 1
    private final String In = "Штатныйвход";
    private final String RadioDeviceIn = "КПП-2радиодом";
    private final String RadioDeviceOut = "ТурникетРадиодом"; // 4
    private final String TeleDeice = "ТурникетКПП";
    private final String TeleDom = "ТурникетТеледом";
    private final String TeleMesgOut = "Предоставлениедоступанавыход";
    private final String TeleMesgIn = "Предоставлениедоступанавход";
    private String filename = "";
    private  Long normTimeMonth;
    private String fileName1C;
    private boolean file1cExis = false;
    private final String RegSpace = "\\s+";
    private DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private LocalDateTime firstDate;
    private LocalDateTime endDate;
    private final Long eatStnadartTime = 45L;
    private final Long eatBigTime = 60L;
    private Map<String, EmployeeVisit> employeemap = new LinkedHashMap<String, EmployeeVisit>();
    private List<StaffNotWork> listStaf;
    
    public static Long second = 0L;

    public void setFileName1C(String fileName1C) {
        this.fileName1C = fileName1C;
    }
    
    public  void setNormTimeMonth(Long hour){
            this.normTimeMonth = hour * 60;
        }   
        
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public LocalDate getFirstDate() {
        return firstDate.toLocalDate();
    }

    public void setFirstDate(LocalDateTime fDate) {
        if(this.firstDate.isAfter(fDate)){
            this.firstDate = fDate;
        }
    }

    public LocalDate getEndDate() {
        return endDate.toLocalDate();
    }

    public void setEndDate(LocalDateTime eDate) {
        if(this.endDate.isBefore(eDate)){
            this.endDate = eDate;
        }
    }
    private boolean inReader(XMLStreamReader reader) {
    	String AtrValue1 = reader.getAttributeValue(1).replaceAll(RegSpace, "");
    	String AtrValue3 = reader.getAttributeValue(3).replaceAll(RegSpace, "");
	return (In.equalsIgnoreCase(AtrValue1) && RadioDeviceOut.equalsIgnoreCase(AtrValue3))
            || (TeleDeice.equalsIgnoreCase(AtrValue3) && TeleMesgIn.equalsIgnoreCase(AtrValue1))
            || (TeleDom.equalsIgnoreCase(AtrValue3) && TeleMesgIn.equalsIgnoreCase(AtrValue1));
    }

    private boolean outReader(XMLStreamReader reader) {
        String AtrValue1 = reader.getAttributeValue(1).replaceAll(RegSpace, "");
	String AtrValue3 = reader.getAttributeValue(3).replaceAll(RegSpace, "");
	return (Out.equalsIgnoreCase(AtrValue1) && RadioDeviceOut.equalsIgnoreCase(AtrValue3))
            || (TeleDeice.equalsIgnoreCase(AtrValue3) && TeleMesgOut.equalsIgnoreCase(AtrValue1))
            || (TeleDom.equalsIgnoreCase(AtrValue3) && TeleMesgOut.equalsIgnoreCase(AtrValue1));
    }

    public void printEmployee() {
        for (Map.Entry<String, EmployeeVisit> empl : this.employeemap.entrySet()) {
            EmployeeVisit emplUnit = empl.getValue();
            System.out.println("Сотрудник :");
            System.out.println(emplUnit.getLastName());
            System.out.println(emplUnit.getFirstName());
            System.out.println(emplUnit.getMidleName());
            System.out.println(emplUnit.getDepartament());
            Long fullTime = 0L;
            Long overWork = 0L;
            Long workTime = 0L;
            for (Map.Entry<Integer, DayTime> dayTime : emplUnit.getDayTime().entrySet()) {
                System.out.println("Дата : " + dayTime.getValue().getInput().format(format).toString());
		System.out.print("Вход: " + dayTime.getValue().getInput().getHour() + ":"+ dayTime.getValue().getInput().getMinute());
		System.out.println("   Выход: " + dayTime.getValue().getOutput().getHour() + ":" + dayTime.getValue().getOutput().getMinute());
		if(dayTime.getValue().getOutput().getHour() != 0) {
		Long time = (dayTime.getValue().getOutput().toInstant(ZoneOffset.UTC).getEpochSecond()
		- dayTime.getValue().getInput().toInstant(ZoneOffset.UTC).getEpochSecond()) / 60;
		System.out.println("Отработанное время: " + time / 60 + " часов и " + time % 60 + "минут");
		
		System.out.println();
                    if(time >= 8*60) {
                        fullTime += time - 45;
			workTime +=  8*60;
                    }
		}
            }
            overWork = fullTime - this.normTimeMonth;
            System.out.println("Отработано за период " + fullTime/60 + "часов и " + fullTime%60 + "минут");
            System.out.println("Переработка за период составила " + overWork/60 + "часов и " + overWork%60 + "минут");
        }
    }

    public List<DTOExelTabel>  DTOExelEmployee() {
        List<DTOExelTabel> lDtoTbl = new LinkedList<>();
            for (Map.Entry<String, EmployeeVisit> empl : this.employeemap.entrySet()) {
                DTOExelTabel dtUnit = new DTOExelTabel();
		EmployeeVisit emplUnit = empl.getValue();
		System.out.println("Сотрудник :");
		System.out.println(emplUnit.getLastName());
		System.out.println(emplUnit.getFirstName());
		System.out.println(emplUnit.getMidleName());
		System.out.println(emplUnit.getDepartament());
                dtUnit.setNameFML(emplUnit.getLastName(), emplUnit.getFirstName(), emplUnit.getMidleName());
                dtUnit.setOrgUnit(emplUnit.getDepartament());
                if(this.file1cExis){
                    for(StaffNotWork nws : this.listStaf){
                        if(nws.getFLM().equalsIgnoreCase(dtUnit.getNameFML())){
                            dtUnit.setStaffOnC(nws);
                            dtUnit.setFile1CExis(true);
                        }
                    }
                }
		Long fullTime = 0L;
		Long overWork = 0L;
		Long workTime = 0L;
                Long eatSumm = 0L;
                    for (Map.Entry<Integer, DayTime> dayTime : emplUnit.getDayTime().entrySet()) {
                        this.setFirstDate(dayTime.getValue().getInput());
                        this.setEndDate(dayTime.getValue().getOutput());
			System.out.println("Дата : " + dayTime.getValue().getInput().format(format).toString());
			System.out.print("Вход: " + dayTime.getValue().getInput().getHour() + ":"+ dayTime.getValue().getInput().getMinute());
			System.out.println("   Выход: " + dayTime.getValue().getOutput().getHour() + ":" + dayTime.getValue().getOutput().getMinute());
			dtUnit.setInputTime(dayTime.getValue().getInput().toInstant(ZoneOffset.UTC).getEpochSecond(), AlarmTime.WORK);
                        dtUnit.setOutputTime(dayTime.getValue().getOutput().toInstant(ZoneOffset.UTC).getEpochSecond(), AlarmTime.WORK);
                                
                        if(dayTime.getValue().getOutput().getHour() != 0) {
			    Long time = (dayTime.getValue().getOutput().toInstant(ZoneOffset.UTC).getEpochSecond()
                            - dayTime.getValue().getInput().toInstant(ZoneOffset.UTC).getEpochSecond()) / 60;
			    System.out.println("Отработанное время: " + time / 60 + " часов и " + time % 60 + "минут");
                                    
			    System.out.println();
			    if(time >= 4*60 && time <= 8*60 +30) {
				fullTime += time - this.eatStnadartTime;
                                dtUnit.setDiferentTime(time - this.eatStnadartTime, dayTime.getValue().getInput().toInstant(ZoneOffset.UTC).getEpochSecond(), AlarmTime.WORK);
                                workTime +=  8*60;
                                eatSumm += this.eatStnadartTime;
			   }else if(time < 4*60 && time > 0){
                                fullTime += time;
                                dtUnit.setDiferentTime(time, dayTime.getValue().getInput().toInstant(ZoneOffset.UTC).getEpochSecond(), AlarmTime.WORK);
                                workTime +=  4*60;
                            }else if(time > (8*60 + 30)){
                                fullTime += time - this.eatBigTime;
                                dtUnit.setDiferentTime(time - this.eatBigTime, dayTime.getValue().getInput().toInstant(ZoneOffset.UTC).getEpochSecond(), AlarmTime.WORK);
                                workTime +=  8*60;
                                eatSumm += this.eatBigTime;
                            }
			}
                                
                    }
                    overWork = fullTime - this.normTimeMonth;
                    System.out.println("Отработано за период " + fullTime/60 + "часов и " + fullTime%60 + "минут");
                    System.out.println("Переработка за период составила " + overWork/60 + "часов и " + overWork%60 + "минут");
                    dtUnit.setSumWorkTime(fullTime / 60 + ":" + ((fullTime % 60 < 10) ? "0" + fullTime % 60 : fullTime % 60));
                    if(overWork > 0) {
                       	dtUnit.setOverWorkTime(overWork / 60 + ":" + ((overWork % 60 < 10) ? "0" + overWork % 60 : overWork % 60));
                    }
                    dtUnit.setSumEatTime(eatSumm / 60 + ":" + ((eatSumm % 60 < 10) ? "0" + eatSumm % 60 : eatSumm % 60));
                    dtUnit.setNormWorkTime(this.normTimeMonth / 60 + ":" + this.normTimeMonth % 60);
                    Long notWork = this.normTimeMonth  -  fullTime;
                    if(notWork > 0){
                        dtUnit.setNotWorks(notWork / 60 + ":" + ((notWork % 60 < 10)?  "0" + notWork % 60 : notWork % 60) );
                    }else{
                        dtUnit.setNotWorks("");
                    }
                        lDtoTbl.add(dtUnit);
		}
                return lDtoTbl;
	}


	public void start() {
		String firstName;
		String midleName;
		String lastName;
		String departament;
		String key;
                this.firstDate = LocalDateTime.MAX;
                this.endDate = LocalDateTime.MIN;
                HashSet<String> dept = new LinkedHashSet<String>();
                Text1CParser txtPars = new Text1CParser(); 
                if(!this.fileName1C.trim().isEmpty()){
                    txtPars.setFileName(this.fileName1C);
                    txtPars.loadFile();
                    this.listStaf = txtPars.getStaff();
                    this.file1cExis = true;
                }                
                
		try {
			InputStream input = new FileInputStream(this.filename);
			XMLInputFactory factory = XMLInputFactory.newInstance();
			try {
				XMLStreamReader reader = factory.createXMLStreamReader(input);
				int event = reader.getEventType();
				while (true) {
					if (XMLStreamConstants.START_ELEMENT == event) {
						// System.out.println("Start Element");
						dept.add(reader.getAttributeValue(12));
						if ("ROW".equalsIgnoreCase(reader.getLocalName())) {
							firstName = reader.getAttributeValue(10).trim();
							midleName = reader.getAttributeValue(8).trim();
							lastName = reader.getAttributeValue(9).trim();
							departament = reader.getAttributeValue(12);
							//key = lastName + firstName + midleName + departament.replaceAll(RegSpace, "");
                                                        key = lastName + firstName + midleName;
							EmployeeVisit emplV = null;
							// Выход
							if (this.outReader(reader)) {
								String time = reader.getAttributeValue(2).substring(10).trim();
								if (time.regionMatches(1, ":", 0, 1)) {
									time = "0" + time;
								}
								LocalTime lt = LocalTime.parse(time);
								LocalDateTime ld = LocalDateTime
										.of(LocalDate.parse(reader.getAttributeValue(2).substring(0, 10), format), lt);

								if (employeemap.containsKey(key)) {
									emplV = employeemap.get(key);
								} else {
									emplV = new EmployeeVisit(firstName, midleName, lastName, departament);
								}
								emplV.setDayTimeOut(ld);
							}
							// Вход
							if (this.inReader(reader)) {
								String time = reader.getAttributeValue(2).substring(10).trim();
								if (time.regionMatches(1, ":", 0, 1)) {
									time = "0" + time;
								}
								LocalTime lt = LocalTime.parse(time);
								LocalDateTime ld = LocalDateTime
										.of(LocalDate.parse(reader.getAttributeValue(2).substring(0, 10), format), lt);
								if (employeemap.containsKey(key)) {
									emplV = employeemap.get(key);
								} else {
									emplV = new EmployeeVisit(firstName, midleName, lastName, departament);
								}
								emplV.setDayTimeIn(ld);
							}
							if (emplV != null) {
								if (!this.employeemap.containsKey(key)) {
									this.employeemap.put(key, emplV);
								}
							}
						}
					}

					if (!reader.hasNext()) {
						break;
					}

					event = reader.next();
				}

			} catch (XMLStreamException ex) {
				Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
			}

		} catch (IOException ex) {
			Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
