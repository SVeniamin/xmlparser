package samara.exel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import samara.DTO.DTOExel;
import samara.DTO.PairData;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *���� �������� � ������ � ������ �������� �������� ����������
 * @author admin
 */
public class ExelWriter {

    private XSSFWorkbook book;
    private XSSFSheet sheet;
    private final static String nameFile = "/home/venia/ExelWork.xlsx";
    private FileOutputStream fOut;
    public void createFile() {
        book = new XSSFWorkbook();
        sheet = book.createSheet("������ ������");
    }

    public void closeFile() {
        try {
        	fOut = new FileOutputStream(this.nameFile);
            book.write(fOut);
            
            System.out.println("AppContext = " + new File(".").getAbsoluteFile());
            
        } catch (IOException ex) {
            Logger.getLogger(ExelWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
/**
 *  �������� ����� �������� ���������� 
 * @param first ��������� ����
 * @param Range ���������� ����
 */
    public void writeHead(LocalDate first, Integer Range) {
        XSSFRow row = sheet.createRow(0);
        row.setHeight((short)560);
        
        LocalDate end = first.plusDays(Range);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM");
        XSSFFont font = book.createFont();
        font.setFontHeight((short)300);
        font.setBold(true);
        font.setColor((short)0);
        BorderStyle bstyle = BorderStyle.THIN;
        XSSFCellStyle style = book.createCellStyle();
        style.setFont(font);
        
        style.setBorderBottom(bstyle);
        style.setBorderTop(bstyle);
        style.setBorderLeft(bstyle);
        style.setBorderBottom(bstyle);
        style.setBorderRight(bstyle);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        style.setBottomBorderColor(IndexedColors.BLUE.getIndex());
        style.setTopBorderColor(IndexedColors.BLUE.getIndex());
        style.setLeftBorderColor(IndexedColors.BLUE.getIndex());
        style.setRightBorderColor(IndexedColors.BLUE.getIndex());
        XSSFCell cell = row.createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue("���");
        //style.setFillForegroundColor((short)50);
        sheet.autoSizeColumn(0);
        for (int i = 1; !first.equals(end); first = first.plusDays(1), i++) {
            XSSFCell celdata = row.createCell(i);
            celdata.setCellStyle(style);
            celdata.setCellValue(first.format(formatter));
            sheet.autoSizeColumn(i);
        }

        
    }
/**
 * ������ ������ � ��������� 
 * @param First ��������� ���� 
 * @param Range ���������� ����
 * @param numRows ����� ������ ��� ������ ������� � 1
 * @param dt ������ � ���������� � �������
 */
    public void writeRow(LocalDate First, Integer Range, Integer numRows, DTOExel dt) {
        
        LocalDate end = First.plusDays(Range);
        XSSFRow row = sheet.createRow(numRows);
         row.setHeight((short)560);
        XSSFCell celName = row.createCell(0);
        celName.setCellValue(dt.getEmployeeName());
        sheet.autoSizeColumn(0);
        for(int i = 1; !First.equals(end);First = First.plusDays(1), i++){
            XSSFCell tCell = row.createCell(i);
            PairData data = dt.getDataTime().get(First.getDayOfYear() + First.getYear());
            String CelIn = new String("");
            if(data != null)
            	CelIn = data.getInput() + "\n" + data.getInput();
            tCell.setCellValue(CelIn);
            sheet.autoSizeColumn(i);
        }
    }
}
