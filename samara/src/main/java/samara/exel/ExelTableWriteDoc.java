package samara.exel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * модуль для записи данных в exel файл табеля работы из приложения
 */
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import samara.DTO.DTOExelTabel;
import samara.constant.AlarmTime;

public class ExelTableWriteDoc {
        private  String fileName;
	    private final static String sheetName = "табель учета рабочего времени";
	    private FileOutputStream fileExel;
	    private String nameOrgUnit;
	    private Map<String, DTOExelTabel> employeeWork;
	    private XSSFWorkbook book;
	    private XSSFSheet sheet;
	    //опоздавший
	    private XSSFCellStyle styleBadTime;
	    //Отпуск
	    private XSSFCellStyle styleLeaveTime;
	    //Больничный
	    private XSSFCellStyle styleSikTime;
	    //Командировка
	    private XSSFCellStyle styleAssignment;
	    //рабочий стиль
	    private XSSFCellStyle styleWork;
	    //Заголовок
	    private XSSFCellStyle styleHeader;
            
            public void setFileName(String fileName) {
                this.fileName = fileName;
            }
	    private void styleHeaderInitial(){
	        XSSFFont font = book.createFont();
	        font.setFontHeight((short)300);
	        font.setBold(true);
	        font.setColor((short)0);
	        BorderStyle bstyle = BorderStyle.THIN;
	        XSSFCellStyle style = book.createCellStyle();
	        this.styleHeader = book.createCellStyle();
	        styleHeader.setFont(font);
	        
	        styleHeader.setBorderBottom(bstyle);
	        styleHeader.setBorderTop(bstyle);
	        styleHeader.setBorderLeft(bstyle);
	        styleHeader.setBorderBottom(bstyle);
	        styleHeader.setBorderRight(bstyle);
	        styleHeader.setAlignment(HorizontalAlignment.CENTER);
	        styleHeader.setVerticalAlignment(VerticalAlignment.CENTER);

	        styleHeader.setBottomBorderColor(IndexedColors.BLUE.getIndex());
	        styleHeader.setTopBorderColor(IndexedColors.BLUE.getIndex());
	        styleHeader.setLeftBorderColor(IndexedColors.BLUE.getIndex());
	        styleHeader.setRightBorderColor(IndexedColors.BLUE.getIndex());
	    }
	    private void styleRowInitial(){
	        XSSFFont fontWork = book.createFont();
	        fontWork.setColor((short)0);
	        XSSFFont fontBadTime = book.createFont();
	        XSSFFont fontSick = book.createFont();
	        XSSFFont fontAssignment = book.createFont();
	        
	        BorderStyle bstyle = BorderStyle.THIN;
	        this.styleWork = book.createCellStyle();
	        this.styleAssignment = book.createCellStyle();
	        this.styleBadTime = book.createCellStyle();
	        this.styleLeaveTime = book.createCellStyle();
	        this.styleSikTime = book.createCellStyle();
	        
	        //установка основноко стиля
	        this.styleWork.setBorderBottom(bstyle);
	        this.styleWork.setBorderTop(bstyle);
	        this.styleWork.setBorderLeft(bstyle);
	        this.styleWork.setBorderRight(bstyle);
	        this.styleWork.setAlignment(HorizontalAlignment.CENTER);
	        this.styleWork.setVerticalAlignment(VerticalAlignment.CENTER);;
	        this.styleWork.setFont(fontSick);

	        this.styleWork.setBottomBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleWork.setTopBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleWork.setLeftBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleWork.setRightBorderColor(IndexedColors.BLACK1.getIndex());
	        
	        //Опоздавший
	        this.styleBadTime.setBorderBottom(bstyle);
	        this.styleBadTime.setBorderTop(bstyle);
	        this.styleBadTime.setBorderLeft(bstyle);
	        this.styleBadTime.setBorderRight(bstyle);
	        this.styleBadTime.setAlignment(HorizontalAlignment.CENTER);
	        this.styleBadTime.setVerticalAlignment(VerticalAlignment.CENTER);

	        this.styleBadTime.setBottomBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleBadTime.setTopBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleBadTime.setLeftBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleBadTime.setRightBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleBadTime.setFont(fontBadTime);
	        //Больничный
	        this.styleSikTime.setBorderBottom(bstyle);
	        this.styleSikTime.setBorderTop(bstyle);
	        this.styleSikTime.setBorderLeft(bstyle);
	        this.styleSikTime.setBorderRight(bstyle);
	        this.styleSikTime.setAlignment(HorizontalAlignment.CENTER);
	        this.styleSikTime.setVerticalAlignment(VerticalAlignment.CENTER);

	        this.styleSikTime.setBottomBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleSikTime.setTopBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleSikTime.setLeftBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleSikTime.setRightBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleSikTime.setFont(fontSick);
	        
	        //Командировка
	        this.styleAssignment.setBorderBottom(bstyle);
	        this.styleAssignment.setBorderTop(bstyle);
	        this.styleAssignment.setBorderLeft(bstyle);
	        this.styleAssignment.setBorderRight(bstyle);
	        this.styleAssignment.setAlignment(HorizontalAlignment.CENTER);
	        this.styleAssignment.setVerticalAlignment(VerticalAlignment.CENTER);

	        this.styleAssignment.setBottomBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleAssignment.setTopBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleAssignment.setLeftBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleAssignment.setRightBorderColor(IndexedColors.BLACK1.getIndex());
	        this.styleAssignment.setFont(fontAssignment);
	    }
	    public void createFile() {
	        book = new XSSFWorkbook();
	        sheet = book.createSheet(ExelTableWriteDoc.sheetName);

	        
	    }
	        public void closeFile() {
	        try {
	        	this.fileExel = new FileOutputStream(this.fileName);
	            book.write(this.fileExel);
	            this.fileExel.close();
	        } catch (IOException ex) {
	            Logger.getLogger(ExelWriter.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	        
	    public void writeHead(LocalDate first, LocalDate end) {
	        XSSFRow row = sheet.createRow(0);
	        row.setHeight((short)560);
	        this.styleHeaderInitial();
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM");

	        XSSFCell cell = row.createCell(0);
	        cell.setCellStyle(this.styleHeader);
	        cell.setCellValue("ФИО");
	        //style.setFillForegroundColor((short)50);
	        sheet.autoSizeColumn(0);
	        int iterationNumberColum = 0;
	        for (iterationNumberColum = 1; !first.equals(end); first = first.plusDays(1), iterationNumberColum++) {
	            XSSFCell celdata = row.createCell(iterationNumberColum);
	            celdata.setCellStyle(this.styleHeader);
	            celdata.setCellValue(first.format(formatter));
	            sheet.autoSizeColumn(iterationNumberColum);
	        } 
	        XSSFCell cellSumWorks = row.createCell(iterationNumberColum);
	        cellSumWorks.setCellStyle(this.styleHeader);
	        cellSumWorks.setCellValue("Общ. \n Факт");
	        sheet.autoSizeColumn(iterationNumberColum);
	        iterationNumberColum++;
	        XSSFCell cellSumEat = row.createCell(iterationNumberColum);
	        cellSumEat.setCellStyle(this.styleHeader);
	        cellSumEat.setCellValue("Обеды");
	        sheet.autoSizeColumn(iterationNumberColum);
	        iterationNumberColum++;
	        XSSFCell cellSumNorm = row.createCell(iterationNumberColum);
	        cellSumNorm.setCellStyle(this.styleHeader);
	        cellSumNorm.setCellValue("Норм");
	        sheet.autoSizeColumn(iterationNumberColum);
	        iterationNumberColum++;
	        XSSFCell cellSumNotWork = row.createCell(iterationNumberColum);
	        cellSumNotWork.setCellStyle(this.styleHeader);
	        cellSumNotWork.setCellValue("Не отр. \n Факт");
	        sheet.autoSizeColumn(iterationNumberColum);
	        iterationNumberColum++;
	        XSSFCell cellSumOverWork = row.createCell(iterationNumberColum);
	        cellSumOverWork.setCellStyle(this.styleHeader);
	        cellSumOverWork.setCellValue("Переработка");
	        sheet.autoSizeColumn(iterationNumberColum);
	    }
	    
	    public void wirteOrgUnit(String Org, int numRow) {
	    	XSSFRow row = sheet.createRow(numRow);
	        row.setHeight((short)560);
	        this.styleHeaderInitial();
	        XSSFCell cell = row.createCell(0);
	        cell.setCellStyle(this.styleHeader);
	        cell.setCellValue(Org);
	        sheet.autoSizeColumn(0);
	        
	    }
	    
	    public void writeRow(LocalDate Start, LocalDate End, Integer numRows, DTOExelTabel DExel){
	        this.styleRowInitial();
	        XSSFRow rowfirst = sheet.createRow(numRows);
	        XSSFRow rowsecond = sheet.createRow(numRows + 1);
	        XSSFRow rowthrid = sheet.createRow(numRows + 2);
	       // rowfirst.setHeight((short)560);
	        XSSFCell celName = rowfirst.createCell(0);
	        celName.setCellValue(DExel.getNameFML());
	        sheet.autoSizeColumn(0);
	        sheet.addMergedRegion(new CellRangeAddress(numRows, numRows + 2, 0, 0));

	        celName.setCellStyle(this.styleWork);
	        int iterationNumberColum = 0;
	        for(iterationNumberColum = 1; !Start.equals(End);Start = Start.plusDays(1), iterationNumberColum++){
	            XSSFCell tCellFirst = rowfirst.createCell(iterationNumberColum);
	            tCellFirst.setCellValue(DExel.getInputTimeByData(Start));
	            AlarmTime curentFirstCellAlarm = DExel.getInputTimeAlarmByData(Start);
	            if(curentFirstCellAlarm == null) {
	            	curentFirstCellAlarm = AlarmTime.BADTIME;
	            }
	            switch(curentFirstCellAlarm){
	                case ASSIGNMENT : tCellFirst.setCellStyle(this.styleAssignment);
	                                    break;
	                case BADTIME : tCellFirst.setCellStyle(this.styleBadTime);
	                                    break;
	                case LEAVE : tCellFirst.setCellStyle(this.styleLeaveTime);
	                                break;
	                case SICK : tCellFirst.setCellStyle(this.styleSikTime);
	                                break;
	                default: tCellFirst.setCellStyle(this.styleWork);
	            }
	            
	            XSSFCell tCellSecond = rowsecond.createCell(iterationNumberColum);
	            tCellSecond.setCellValue(DExel.getOutputRealTimeByData(Start));
	            AlarmTime curentSecontCellAlarm = DExel.getUotputAlarmByData(Start);
                    if(curentSecontCellAlarm == null){
                        curentSecontCellAlarm = AlarmTime.WORK;
                    }
	            switch(curentSecontCellAlarm){
	                case ASSIGNMENT : tCellSecond.setCellStyle(this.styleAssignment);
	                                    break;
	                case BADTIME : tCellSecond.setCellStyle(this.styleBadTime);
	                                    break;
	                case LEAVE : tCellSecond.setCellStyle(this.styleLeaveTime);
	                                break;
	                case SICK : tCellSecond.setCellStyle(this.styleSikTime);
	                                break;
	                default: tCellSecond.setCellStyle(this.styleWork);
	            }

	            XSSFCell tCellThree = rowthrid.createCell(iterationNumberColum);
	            tCellThree.setCellValue(DExel.getRealDifferentTimeByData(Start));
	            AlarmTime curentThreeCellAlarm = DExel.getUotputAlarmByData(Start);
	            switch(curentSecontCellAlarm){
	                case ASSIGNMENT : tCellThree.setCellStyle(this.styleAssignment);
	                                    break;
	                case BADTIME : tCellThree.setCellStyle(this.styleBadTime);
	                                    break;
	                case LEAVE : tCellThree.setCellStyle(this.styleLeaveTime);
	                                break;
	                case SICK : tCellThree.setCellStyle(this.styleSikTime);
	                                break;
	                default: tCellThree.setCellStyle(this.styleWork);
	            }            
	            
	            sheet.autoSizeColumn(iterationNumberColum);
	        }
	        
	        XSSFCell cellSumWorks = rowfirst.createCell(iterationNumberColum);
	        cellSumWorks.setCellStyle(this.styleWork);
	        cellSumWorks.setCellValue(DExel.getSumWorkTime());
	        sheet.autoSizeColumn(iterationNumberColum);
	        sheet.addMergedRegion(new CellRangeAddress(numRows, numRows + 2, iterationNumberColum, iterationNumberColum));
	        iterationNumberColum++;
	        XSSFCell cellSumEat = rowfirst.createCell(iterationNumberColum);
	        cellSumEat.setCellStyle(this.styleWork);
	        cellSumEat.setCellValue(DExel.getSumEatTime());
	        sheet.addMergedRegion(new CellRangeAddress(numRows, numRows + 2, iterationNumberColum, iterationNumberColum));
	        sheet.autoSizeColumn(iterationNumberColum);
	        iterationNumberColum++;
	        XSSFCell cellSumNorm = rowfirst.createCell(iterationNumberColum);
	        cellSumNorm.setCellStyle(this.styleWork);
	        cellSumNorm.setCellValue(DExel.getNormWorkTime());
	        sheet.addMergedRegion(new CellRangeAddress(numRows, numRows + 2, iterationNumberColum, iterationNumberColum));
	        sheet.autoSizeColumn(iterationNumberColum);
	        iterationNumberColum++;
	        XSSFCell cellSumNotWork = rowfirst.createCell(iterationNumberColum);
	        cellSumNotWork.setCellStyle(this.styleBadTime);
	        cellSumNotWork.setCellValue(DExel.getNotWorks());
	        sheet.addMergedRegion(new CellRangeAddress(numRows, numRows + 2, iterationNumberColum, iterationNumberColum));
	        sheet.autoSizeColumn(iterationNumberColum);
	        iterationNumberColum++;
	        XSSFCell cellSumOverWork = rowfirst.createCell(iterationNumberColum);
	        cellSumOverWork.setCellStyle(this.styleWork);
	        cellSumOverWork.setCellValue(DExel.getOverWorkTime());
	        sheet.addMergedRegion(new CellRangeAddress(numRows, numRows + 2, iterationNumberColum, iterationNumberColum));
	        sheet.autoSizeColumn(iterationNumberColum);
	        
	    }
}
