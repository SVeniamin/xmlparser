package samara.constant;
/**
 * Перечисление используется для установки стилей ячеек Exel документа (Табель рабочего времени)
 * @author admin
 *
 */
public enum AlarmTime {
    /**
     * отпуск
     */
    LEAVE,
    /**
     * Опоздал или раньше ушел
     */
    BADTIME,
    /**
     * Больничный
     */
    SICK,
    /**
     * Командировка
     */
    ASSIGNMENT,
    /**
     * 
     */
    WORK
}
