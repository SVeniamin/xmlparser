package samara.model;
/**
 * клас предназначенный для сохранения и обработки времени в ExelTableWriterDoc
 */
import samara.constant.AlarmTime;

public class DataTimeInputUnitExel {
	   private AlarmTime alarmRealInputTime;
	   //реальное время входа
	   private String realInputTime;
	    private AlarmTime alarmRealOutputTime;
	    //реальное время выхода
	    private String realOutputTime;
	    private AlarmTime alarmDifferentTime;
	    //реальное отработанное количество часов 
	    private String differentTime;

	    public String getDifferentTime() {
	        return differentTime;
	    }

	    public void setDifferentTime(String differentTime) {
	        this.differentTime = differentTime;
	    }

	    public AlarmTime getAlarmRealInputTime() {
	        return alarmRealInputTime;
	    }

	    public void setAlarmRealInputTime(AlarmTime alarmRealInputTime) {
	        this.alarmRealInputTime = alarmRealInputTime;
	    }

	    public AlarmTime getAlarmRealOutputTime() {
	        return alarmRealOutputTime;
	    }

	    public void setAlarmRealOutputTime(AlarmTime alarmRealOutputTime) {
	        this.alarmRealOutputTime = alarmRealOutputTime;
	    }

	    public AlarmTime getAlarmDifferentTime() {
	        return alarmDifferentTime;
	    }

	    public void setAlarmDifferentTime(AlarmTime alarmDifferentTime) {
	        this.alarmDifferentTime = alarmDifferentTime;
	    }
	    private String realDifferentTime;

	    public String getRealInputTime() {
	        return realInputTime;
	    }

	    public void setRealInputTime(String realInputTime) {
	        this.realInputTime = realInputTime;
	    }

	    public String getRealOutputTime() {
	        return realOutputTime;
	    }

	    public void setRealOutputTime(String realOutputTime) {
	        this.realOutputTime = realOutputTime;
	    }

	    public String getRealDifferentTime() {
	        return realDifferentTime;
	    }

	    public void setRealDifferentTime(String realDifferentTime) {
	        this.realDifferentTime = realDifferentTime;
	    }
	    
}
