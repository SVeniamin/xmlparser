/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package samara.text1CParser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.*;

/**
 *
 * @author Samoylov V V
 */
public class Text1CParser {
    private String fileName;
    private final String startString = "^ГТРК.";
    private List<StaffNotWork> staff;
    private boolean bodyDoc = false;

    public List<StaffNotWork> getStaff() {
        return staff;
    }
    public String getFileName() {
        return fileName;
    }
    
    private Pattern template = Pattern.compile(this.startString);
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }



    public String getStartString() {
        return startString;
    }

 
    
    
    public void loadFile(){    
                try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(this.fileName), StandardCharsets.UTF_8))){
            this.staff = new LinkedList<>();
            String line;
            bodyDoc = false;
            while ((line = reader.readLine()) != null) {
                Matcher mach = this.template.matcher(line);
                if(!mach.lookingAt() && !bodyDoc){
                    continue;
                }
                if(mach.lookingAt()){
                    bodyDoc = true;
                    continue;
                }
                if(bodyDoc){
                    int indexLastName = line.indexOf(" ", 1);
                    int indexFirstName = line.indexOf(" ",indexLastName + 2);
                    int indexMidleName = line.indexOf("\t", indexFirstName + 2);
                    int indexMessage = line.indexOf("\t", indexMidleName + 2);
                    int indexStartData1 = line.indexOf("\t", indexMessage + 1);
                    indexStartData1 = line.indexOf("\t", indexStartData1 + 1);
                    int indexStartData2 = line.indexOf("\t", indexStartData1 + 1);
                    String EndDate = line.substring(indexStartData2);
                    if(EndDate.indexOf(":") > 0){
                        int indexSepar = EndDate.indexOf(":");
                        EndDate = EndDate.substring(indexSepar - 1);
                    }
                    if(EndDate.matches("0:00:00")){
                        EndDate = "01.01.1970";
                    }
                    EndDate = EndDate.replaceAll("[a-zA-Zа-яА-Я\\s]", "");
                    this.staff.add(new StaffNotWork(line.substring(0, indexLastName), line.substring(indexLastName + 1, indexFirstName),
                            line.substring(indexFirstName + 1, indexMidleName ), line.substring(indexMidleName + 1, indexMessage), 
                            line.substring(indexStartData1, indexStartData2), EndDate )) ;
                    
                }
            }
        } catch (IOException e) {
            // log error
        }
  
    }
    

}
    
