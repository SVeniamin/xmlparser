/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package samara.text1CParser;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Samoylov V.V
 */
public class StaffNotWork {
    private String firstName;
    private String lastName;
    private String midleName;
    private String mesageWork;
    private LocalDate startDate;
    private LocalDate endDate;
    private DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private Map<Integer, String> messageMap;

    public Map<Integer, String> getMessageMap() {
        return messageMap;
    }
     private Map<Integer, String> notWork;
    public StaffNotWork(String lastName,   String firstName,String midleName,  String Message, String StartDate, String  EndDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.midleName = midleName;
        this.mesageWork = Message.replace( ' ', '\n');
        this.startDate = LocalDate.parse(StartDate.trim(), format);
        this.endDate = LocalDate.parse(EndDate.trim(), format);
        this.messageMap = new LinkedHashMap<>();
        if(endDate.equals(LocalDate.parse("01.01.1970".trim(), format))){
            endDate = LocalDate.ofYearDay(startDate.getYear(), startDate.lengthOfMonth());
        }
        if(startDate.equals(LocalDate.parse("01.01.1970".trim(), format))){
            startDate = LocalDate.ofYearDay(endDate.getYear(), 1);
        }
        if(endDate.getYear() - LocalDate.now().getYear() > 1){
            startDate = LocalDate.ofYearDay(LocalDate.now().getYear(), 1);
        }
        // ��� ����������� ����� ��������� ���� ���������� �� ������ ��������� � endDate ���� ����
        this.endDate = this.endDate.plusDays(1);
        for(LocalDate ld = startDate.minusDays(0L);ld.isBefore(endDate); ld = ld.plusDays(1)){
            this.messageMap.put(ld.getDayOfYear(), Message);
        }
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public String getMesageWork() {
        return mesageWork;
    }

    public StaffNotWork() {
        notWork = new LinkedHashMap<>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMidleName() {
        return midleName;
    }

    public void setMidleName(String midleName) {
        this.midleName = midleName;
    }

    public Map<Integer, String> getNotWork() {
        return notWork;
    }

    public void setNotWork(Map<Integer, String> notWork) {
        this.notWork = notWork;
    }
    
    public void setStaffDate(LocalDate ld, String value){
        Integer dt = ld.getDayOfYear();
        if(this.notWork.containsKey(dt)){
            return;
        }
        
        this.notWork.put(dt, value);
    }
    
    public String getStaffDate(LocalDate ld){
        return this.notWork.get(ld.getDayOfYear());
    }
    
    public String getFLM(){
        StringBuilder st = new StringBuilder(this.lastName);
        st.append(" ");
        if(this.firstName != null && !this.firstName.isEmpty()){
            st.append(this.firstName.charAt(0));
            st.append(".");
        }
        if(this.midleName != null && !this.midleName.isEmpty()){
            st.append(this.midleName.charAt(0));
            st.append(".");
        }
        
        
        return  st.toString();
    }
    public String getLFM(){
        return this.lastName + this.firstName + this.midleName;
    }
}
