package samara.DTO;
/**
 * клас для обмена данными между приложением и модулем записи в exel результата табеля
 */
import samara.text1CParser.StaffNotWork;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

import samara.constant.AlarmTime;
import samara.model.DataTimeInputUnitExel;

public class DTOExelTabel {
	  // id from BD
    private Integer idEmployee;
    //Ф.И.О
    private String nameFML;
    //key дата в месяце работы строковое значение
    //Класс с полями время входа, время выхода и отработанное время
    private Map<String, DataTimeInputUnitExel> timeUnit;
    //не отработанное количество часов (прогулы)
    private String notWorks;
    //сумма всего времени отработанного за указанный период.
    private String sumWorkTime;
    //Сумма времени потраченого на обеды за указанный период
    private String sumEatTime;
    //Норма рабочего времени
    private String normWorkTime;
    //Переработка количество часов
    private String overWorkTime;
    private String orgUnit;
    // данные о сотруднике из 1С
    private  StaffNotWork staffOnC;
    
    private boolean file1CExis = false;

    public void setFile1CExis(boolean file1CExis) {
        this.file1CExis = file1CExis;
    }
    
    public StaffNotWork getStaffOnC() {
        return staffOnC;
    }

    public void setStaffOnC(StaffNotWork staffOnC) {
        this.staffOnC = staffOnC;
    }

	public String getOrgUnit() {
		return orgUnit;
	}

	public void setOrgUnit(String orgUnit) {
		this.orgUnit = orgUnit;
	}
    private final  DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm");
    private final  DateTimeFormatter formatKey = DateTimeFormatter.ofPattern("dd:MM:YYYY");
    
    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getNameFML() {
        return nameFML;
    }

    public void setNameFML(String FirstName, String MidleName, String LastName) {
        StringBuilder st = new StringBuilder(LastName);
        st.append(" ");
        if(FirstName != null && !FirstName.isEmpty()){
            st.append(FirstName.charAt(0));
            st.append(".");
        }
        if(MidleName != null && !MidleName.isEmpty()){
            st.append(MidleName.charAt(0));
            st.append(".");
        }
        
        
        this.nameFML = st.toString();
    }

    public Map<String, DataTimeInputUnitExel> getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(Map<String, DataTimeInputUnitExel> timeUnit) {
        this.timeUnit = timeUnit;
    }

    public String getNotWorks() {
        return notWorks;
    }

    public void setNotWorks(String notWorks) {
        this.notWorks = notWorks;
    }

    public String getSumWorkTime() {
        return sumWorkTime;
    }

    public void setSumWorkTime(String sumWorkTime) {
        this.sumWorkTime = sumWorkTime;
    }

    public String getSumEatTime() {
        return sumEatTime;
    }

    public void setSumEatTime(String sumEatTime) {
        this.sumEatTime = sumEatTime;
    }

    public String getNormWorkTime() {
        return normWorkTime;
    }

    public void setNormWorkTime(String normWorkTime) {
        this.normWorkTime = normWorkTime;
    }

    public String getOverWorkTime() {
        return overWorkTime;
    }

    public void setOverWorkTime(String overWorkTime) {
        this.overWorkTime = overWorkTime;
    }
    public DataTimeInputUnitExel getUnitDataTimeByKey(String key){
        return this.timeUnit.get(key);
    }

    public void setInputTime(Long InputSecond, AlarmTime alarm){
        Instant inputInstant  = Instant.ofEpochSecond(InputSecond);
        LocalDateTime timeInput = LocalDateTime.ofInstant(inputInstant, ZoneOffset.UTC);

        DataTimeInputUnitExel dI;
        if(this.timeUnit.containsKey(timeInput.format(this.formatKey).toString())){
             dI = this.timeUnit.get(timeInput.format(this.formatKey));
        }else{
            dI = new DataTimeInputUnitExel();
        }
        dI.setRealInputTime(timeInput.format(this.format));
        dI.setAlarmRealInputTime(alarm);
        this.timeUnit.put(timeInput.format(this.formatKey), dI);
        
    }
    public void setMessage(Long InputSecond, String Message){
         Instant inputInstant  = Instant.ofEpochSecond(InputSecond);
        LocalDateTime timeInput = LocalDateTime.ofInstant(inputInstant, ZoneOffset.UTC);

        DataTimeInputUnitExel dI;
        if(this.timeUnit.containsKey(timeInput.format(this.formatKey).toString())){
             dI = this.timeUnit.get(timeInput.format(this.formatKey));
        }else{
            dI = new DataTimeInputUnitExel();
        }
        dI.setRealInputTime(Message);
        this.timeUnit.put(timeInput.format(this.formatKey), dI);
    }
    public DTOExelTabel() {
        this.timeUnit = new LinkedHashMap<>();
    }

    public void setOutputTime(Long OutputSecond, AlarmTime alarm){
        Instant outputInstant  = Instant.ofEpochSecond(OutputSecond);
        LocalDateTime timeOutput = LocalDateTime.ofInstant(outputInstant, ZoneOffset.UTC);
        DataTimeInputUnitExel dI;
        if(this.timeUnit.containsKey(timeOutput.format(this.formatKey).toString())){
             dI = this.timeUnit.get(timeOutput.format(this.formatKey));
        }else{
            dI = new DataTimeInputUnitExel();
        }
        dI.setRealOutputTime(timeOutput.format(this.format));
        dI.setAlarmRealOutputTime(alarm);
        this.timeUnit.put(timeOutput.format(this.formatKey), dI);
        
    }    
 
    public void setDiferentTime(Long DiferentMinute, Long DateSecond, AlarmTime alarm){
        Instant diferentInstant  = Instant.ofEpochSecond(DateSecond);
        LocalDateTime timeDiferent = LocalDateTime.ofInstant(diferentInstant, ZoneOffset.UTC);

        DataTimeInputUnitExel dI;
        if(this.timeUnit.containsKey(timeDiferent.format(this.formatKey).toString())){
             dI = this.timeUnit.get(timeDiferent.format(this.formatKey));
             System.err.println("setInput kesh " + timeDiferent.format(this.formatKey ).toString() + " Time : " + DiferentMinute);
        }else{
            dI = new DataTimeInputUnitExel();
        }
        dI.setRealDifferentTime(DiferentMinute / 60 + ":" + DiferentMinute % 60);
        dI.setAlarmDifferentTime(alarm);
        System.err.println("Check DiffTime :" + dI.getRealDifferentTime());
        this.timeUnit.put(timeDiferent.format(this.formatKey).toString(), dI);
        
    }    
    
    public String getInputTimeByData(LocalDate data){
        try{
            return  this.timeUnit.get(data.format(this.formatKey).toString()).getRealInputTime();
        }
        catch(NullPointerException e){
            //TODO доделать вставку сообщения по дате
            String message = "";
            if(this.file1CExis){
                Map<Integer, String> mapMesg = this.staffOnC.getMessageMap();
                message = mapMesg.get(data.getDayOfYear());
            }
            System.err.println("input data exepition DTOExelTable 209 Insert day " + this.nameFML + " : " + e.getLocalizedMessage() + " Data : " + data.format(formatKey).toString());
            return message;
        }
    }
    public AlarmTime getInputTimeAlarmByData(LocalDate data){
        try{
            return this.timeUnit.get(data.format(this.formatKey).toString()).getAlarmRealInputTime();
        }catch(NullPointerException e){
            System.err.println("Input Time Alaarm line DTOExelTable 220 " + e.getLocalizedMessage()+ " Data : " + data.format(this.formatKey).toString());
        }
        return  AlarmTime.WORK;
    }
    
    public String getOutputRealTimeByData(LocalDate data){
        try{
            return this.timeUnit.get(data.format(this.formatKey).toString()).getRealOutputTime();
        }catch(NullPointerException e){
             System.err.println("Output Time  line DTOExelTable 229 " + e.getLocalizedMessage()+ " Data : " + data.format(this.formatKey).toString());
        }
        return "";
    }
    public AlarmTime getUotputAlarmByData(LocalDate data){
        try{
            return this.timeUnit.get(data.format(this.formatKey).toString()).getAlarmRealOutputTime();
        }catch(NullPointerException e){
             System.err.println("Output Time Alaarm line DTOExelTable 237 " + e.getLocalizedMessage()+ " Data : " + data.format(this.formatKey).toString());
        }
        return AlarmTime.WORK;
    }
    public String getDifferentTimeByData(LocalDate data){
        try{      
                return this.timeUnit.get(data.format(this.formatKey).toString()).getDifferentTime();
           }catch(NullPointerException e){
                System.err.println("Different Time  line DTOExelTable 245 " + e.getLocalizedMessage()+ " Data : " + data.format(this.formatKey).toString());
           }  
        return "";
    }
    public String getRealDifferentTimeByData(LocalDate data){
        try{      
                return this.timeUnit.get(data.format(this.formatKey).toString()).getRealDifferentTime();
           }catch(NullPointerException e){
                System.err.println("Different Time  line DTOExelTable 253 " + e.getLocalizedMessage()+ " Data : " + data.format(this.formatKey).toString());
           }  
        return "";
    }
    public AlarmTime getAlarmDifferentTimeByData(LocalDate data){
        try{
                return this.timeUnit.get(data.format(this.formatKey).toString()).getAlarmDifferentTime();
            }catch(NullPointerException e){
             System.err.println("Different Alarm Time  line DTOExelTable 261 " + e.getLocalizedMessage()+ " Data : " + data.format(this.formatKey).toString());
        }
        return AlarmTime.WORK;
    }
}
