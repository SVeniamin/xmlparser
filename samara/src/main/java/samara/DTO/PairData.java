package samara.DTO;

import java.time.LocalDateTime;

/**
 *
 * @author admin
 */
public class PairData {
    private LocalDateTime input;
    private LocalDateTime output;

    public LocalDateTime getInput() {
        return input;
    }

    public void setInput(LocalDateTime input) {
        this.input = input;
    }

    public LocalDateTime getOutput() {
        return output;
    }

    public void setOutput(LocalDateTime output) {
        this.output = output;
    }

    public PairData(LocalDateTime input, LocalDateTime output) {
        this.input = input;
        this.output = output;
    }

    public PairData() {
        this.input = LocalDateTime.now();
        this.output = LocalDateTime.now();
    }
    
}