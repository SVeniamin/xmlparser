package samara.DTO;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author admin
 */
public class DTOExel {
    private String employeeName;
    private Integer id;
    private Map<String, PairData> dataTime;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Map<String, PairData> getDataTime() {
        return dataTime;
    }

    public void setDataTime(Map<String, PairData> dataTime) {
        this.dataTime = dataTime;
    }

    public void addData(String key, Long TimeStampIn, Long TimeStampOut){
        Instant In = Instant.ofEpochSecond(TimeStampIn);
        Instant Ou = Instant.ofEpochMilli(TimeStampOut);
        LocalDateTime ldIn = LocalDateTime.ofInstant(In, ZoneOffset.UTC);
        LocalDateTime ldOu = LocalDateTime.ofInstant(Ou, ZoneOffset.UTC);
        PairData pd = new PairData();
        pd.setInput(ldIn);
        pd.setOutput(ldOu);
        this.dataTime.put(key, pd);
        
    }
    public DTOExel() {
        this.dataTime = new LinkedHashMap<>();
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void setEmployeeFML(String FirstName, String MidleName, String LastName) {
		StringBuilder str = new StringBuilder(LastName);
		str.append(" ");
		if(FirstName.length() > 0)
			str.append(FirstName.charAt(0));
		str.append(".");
		if(MidleName.length() > 0)
			str.append(MidleName.charAt(0));
		str.append(".");
		this.employeeName= str.toString();
	}
    
}
